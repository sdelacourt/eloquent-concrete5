<?php

defined('C5_EXECUTE') or die(_("Access Denied."));

include $_SERVER['DOCUMENT_ROOT'] . DIR_REL . "/packages/eloquent/lib/eloquent/Support/helpers.php";

function illuminateAutoloader($class) {
    //include 'classes/' . $class . '.class.php';
    $parts = explode('\\', $class);
    if (is_array($parts) && count($parts) > 2) {
        array_shift($parts);
        $file = implode('/', $parts);
        include $_SERVER['DOCUMENT_ROOT'] . DIR_REL . "/packages/eloquent/lib/eloquent/" . $file . ".php";
    }
}

spl_autoload_register('illuminateAutoloader');

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

/**
 * A package that allows users to login multiple times to ease account management
 * @author Acato / Simon de la Court
 *
 */
class eloquentPackage extends Package {

    protected $pkgHandle = 'eloquent';
    protected $appVersionRequired = '5.5.0';
    protected $pkgVersion = '0.1';

    /**
     * Enter description here ...
     */
    public function getPackageDescription() {
        return t("Enables Eloquent ORM in Concrete5");
    }

    /**
     * Enter description here ...
     */
    public function getPackageName() {
        return t("Eloquent ORM");
    }

    /**
     * Enter description here ...
     */
    public function install() {
        $pkg = parent::install();
    }

    /**
     * always gets fired on page load, makes sure we are go.
     */
    public function on_start() {
        $capsule = new Capsule;

        $capsule->addConnection([
            'driver' => 'mysql',
            'host' => DB_SERVER,
            'database' => DB_DATABASE,
            'username' => DB_USERNAME,
            'password' => DB_PASSWORD,
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
        ]);


        // Set the event dispatcher used by Eloquent models... (optional)
        $capsule->setEventDispatcher(new Dispatcher(new Container));

        // Set the cache manager instance used by connections... (optional)
        //capsule->setCacheManager(...);
        // Make this Capsule instance available globally via static methods... (optional)
        $capsule->setAsGlobal();

        // Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
        $capsule->bootEloquent();
    }

}
